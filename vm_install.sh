#/bin/bash
sudo apt-get update

# Install LXDE
sudo apt-get install -y xorg lxde
#TODO: add backgrounds folder
#TODO: change background

# Install Other Tools
sudo apt-get install -y wireshark
# Enable root-less wireshark and add mininet
echo "Enabling wireshark group"
echo "wireshark-common wireshark-common/install-setuid boolean true" | sudo debconf-set-selections 
sudo dpkg-reconfigure -f noninteractive wireshark-common
sudo usermod -a -G wireshark mininet
sudo apt-get install -y git-review

# Install ONOS Dependencies
if [ -d onos-setup ]; then
  cd onos-setup
  git pull #already have the repo, just pull the latest
else
  git clone https://bocon13@bitbucket.org/bocon13/onos-setup.git
  cd onos-setup
fi
. config.sh
mkdir $ARCHIVE_DIR
cd scripts
#sudo -u ${ONOS_USER} ./install_zookeeper.sh 
#sudo -u ${ONOS_USER} ./install_cassandra.sh 
#sudo -u ${ONOS_USER} ./install_maven.sh 
#sudo apt-get install -y maven
./install_eclipse.sh
./create_desktop_icons.sh
cd ../..

# Remove wallpaper, change background color, and disable screensaver
sudo sed -i 's/wallpaper_mode=1/wallpaper_mode=0/g' /usr/share/lxde/pcmanfm/LXDE.conf
sudo sed -i 's/desktop_bg=#000000/desktop_bg=#104187/g' /usr/share/lxde/pcmanfm/LXDE.conf
sudo sed -i '/screensaver/d' /etc/xdg/lxsession/LXDE/autostart

#TODO: copy ssh key

# Install ON.Lab root cert
#sudo apt-get install -y libnss3-tools
#sudo wget -c http://gateway.onlab.us/cert/onlab_ca.crt -P /usr/local/share/ca-certificates
#sudo update-ca-certificates
## add cert to Chromium
#certutil -d sql:$HOME/.pki/nssdb -A -t "C,," -n ONLabRootCA -i /usr/local/share/ca-certificates/onlab_ca.crt

# change username and hostname
echo 'mininet:onos' | sudo chpasswd
sudo mv /home/mininet /home/onos
sudo sed -i 's/mininet/onos/g' /etc/group /etc/gshadow /etc/hosts /etc/hostname /etc/passwd /etc/shadow /etc/sudoers

