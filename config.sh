#!/bin/bash

#
ONOS_USER=mininet
ONOS_USER_HOME=/home/${ONOS_USER}

# guest filesystem path
SHARED_DIR=${ONOS_USER_HOME}/onos-setup

## Set directory Setting ##
ZK_DATADIR=/var/lib/zookeeper
CASSANDRA_DATADIR=/var/lib/cassandra
CASSANDRA_LOGDIR=/var/log/cassandra

# File Archives
ARCHIVE_DIR=${SHARED_DIR}/Archives

CASSANDRA_DIR_NAME=apache-cassandra-1.2.4
CASSANDRA_ARCHIVE_NAME=apache-cassandra-1.2.4-bin.tar.gz
CASSANDRA_MIRROR_URL=http://archive.apache.org/dist/cassandra/1.2.4/${CASSANDRA_ARCHIVE_NAME}

ZOOKEEPER_DIR_NAME=zookeeper-3.4.5
ZOOKEEPER_ARCHIVE_NAME=zookeeper-3.4.5.tar.gz
# Pick any mirror from http://www.apache.org/dyn/closer.cgi/zookeeper/
ZOOKEEPER_MIRROR_URL=http://apache.cs.utah.edu/zookeeper/zookeeper-3.4.5/${ZOOKEEPER_ARCHIVE_NAME}

MAVEN_DIR_NAME=apache-maven-3.1.1
MAVEN_ARCHIVE_NAME=apache-maven-3.1.1-bin.tar.gz
MAVEN_MIRROR_URL=http://apache.mirrors.hoobly.com/maven/maven-3/3.1.1/binaries/${MAVEN_ARCHIVE_NAME}

ECLIPSE_URL=http://ftp.osuosl.org/pub/eclipse//technology/epp/downloads/release/kepler/SR1/eclipse-java-kepler-SR1-linux-gtk-x86_64.tar.gz 

# apt package names
JDK_PKG=default-jdk
MAVEN_PKG=maven
FLASK_PKG=python-flask
CURL_PKG=curl
