#!/bin/bash

. `dirname $0`/../config.sh

sudo apt-get -qq update
sudo apt-get -qqy install ${JDK_PKG}

if [ ! -f ${ARCHIVE_DIR}/${ZOOKEEPER_ARCHIVE_NAME} ]; then
  . ${SHARED_DIR}/scripts/fetch_preqs.sh
fi


cd ${ONOS_USER_HOME} # cd to ONOS_USER's home directory 
tar xf ${ARCHIVE_DIR}/${ZOOKEEPER_ARCHIVE_NAME} --no-same-owner

if [ ! -d ${ZK_DATADIR} ]; then
  sudo mkdir -p ${ZK_DATADIR} 
  sudo chown -R `whoami` ${ZK_DATADIR}
fi
