## util.sh ##

function install_pkg () {
  check_inst=`sudo dpkg --get-selections $1 | awk '{print $2}'`
  if [ x$check_inst != "install" ]; then
    echo "Install $1"
    sudo apt-get -qqy install $1
  fi
}
