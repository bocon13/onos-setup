#!/bin/bash

. `dirname $0`/../config.sh
. `dirname $0`/util.sh

PREREQ_PKG="$FLASK_PKG $CURL_PKG"
for p in $PREREQ_PKG; do
  install_pkg $p
done

if [ ! -e ${ONOS_USER_HOME}/ONOS ]; then
  echo "Creating symbolic link from  ${ONOS_USER_HOME}/ONOS to ${SHARED_DIR}/ONOS"
  ln -s ${SHARED_DIR}/ONOS ${ONOS_USER_HOME}/ONOS
  ln -s ${ONOS_USER_HOME}/ONOS/cluster-mgmt/bin ${ONOS_USER_HOME}/bin
  echo "--- Run git pull and compile (refert Readme.md) ---"
fi
