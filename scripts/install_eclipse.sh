#!/bin/bash

. `dirname $0`/../config.sh

if [ ! -f ${ARCHIVE_DIR}/eclipse.tar.gz ]; then
  wget -c -N -O ${ARCHIVE_DIR}/eclipse.tar.gz ${ECLIPSE_URL}
fi

tar xzf ${ARCHIVE_DIR}/eclipse.tar.gz
sudo mv eclipse /opt
sudo ln -s /opt/eclipse/eclipse /usr/bin/eclipse

