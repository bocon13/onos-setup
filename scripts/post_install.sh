#!/bin/bash

. `dirname $0`/../config.sh

## Add hosts ##
sudo sh -c "cat ${SHARED_DIR}/conf_common/hosts >> /etc/hosts"

if [ -d ${HOME}/ONOS/cluster-mgmt/bin ]; then
  ln -s ${HOME}/ONOS/cluster-mgmt/bin ${HOME}/bin
fi
