#! /usr/bin/env python
import pprint
import os
import sys
import subprocess
import json
import argparse
import io
import time
import random
import re

"""
Read config.json (REST server configuration file) and set "controllers" paramter.
If nr_nodes is 1 then set all 8 controllers as "onosdev1", otherwise set it
as onosdev1, .., onosdevX (X=nr_nodes)
"""

if len(sys.argv) != 3:
  print "Usage %s <nr_node> <template config.json>" % sys.argv[0]
  sys.exit(1)

nr_node=int(sys.argv[1])
f = open(sys.argv[2])

conf = json.load(f)
f.close()

controllers=[]
if nr_node == 1:
  for i in range(0,8):
    controllers.append("onosdev1")
else:
  for i in range(1,nr_node+1):
    controllers.append("onosdev%d" % i)
    
conf['controllers']=controllers

print json.dumps(conf,indent=4)
