#! /bin/bash
. `dirname $0`/../config.sh

CONF_TEMPLATE_DIR=`dirname $0`/../conf_template
CONF_COMMON_DIR=`dirname $0`/../conf_common
CONFDIR=`dirname $0`/../conf
ONOS_HOME=`dirname $0`/../ONOS

if [ ! "$#" -eq 1 ]; then
  echo "Usage: $0 <nr_nodes>"
  exit
fi

nr_nodes=$1

rm $CONFDIR/cassandra.yaml.*
rm $CONFDIR/onosrc

echo "Genrating conf files for ${nr_nodes}-node cluster"
# onosrc
if [ ! -f ${CONFDIR}/onosrc ]; then
  touch ${CONFDIR}/onosrc
fi

cat << EOF_CLUSTER_CONF >> $CONFDIR/onosrc 
export M2_HOME=\${HOME}/${MAVEN_DIR_NAME}
export M2=\${M2_HOME}/bin
export PATH=\${M2}:\${PATH}

export CLUSTER=\${HOME}/ONOS/.cluster.hosts
export ONOS_CLUSTER_BASENAME="onosdev"
export ONOS_CLUSTER_NR_NODES=$nr_nodes
export RCMD_CMD="ssh -o StrictHostKeyChecking=no"
export RCP_CMD="scp -o StrictHostKeyChecking=no"
export PATH=\${HOME}/${MAVEN_DIR_NAME}/bin:\${PATH}
EOF_CLUSTER_CONF

# Generating cassandra.yaml in $CONFDIR/cassandra.yaml.{1,2,..}
sCASSANDRA_DATADIR=`echo ${CASSANDRA_DATADIR} | sed 's/\//\\\\\//g'`
cat ${CONF_TEMPLATE_DIR}/cassandra.yaml |\
   awk -v d="${CASSANDRA_DATADIR}/data" 'BEGIN{flag=0}{if($1=="data_file_directories:"){print $0; flag=1}else if(flag==1){flag=0;printf("    - %s\n",d)}else{print $0}}' |\
   sed "s/commitlog_directory:.*/commitlog_directory: ${sCASSANDRA_DATADIR}\/commitlog/g" |\
   sed "s/saved_caches_directory:.*/saved_caches_directory: ${sCASSANDRA_DATADIR}\/saved_caches/g" > ${CONFDIR}/cassandra.yaml
`dirname $0`/token.py $nr_nodes ${CONFDIR}/cassandra.yaml ${CONFDIR}

for i in `seq 1 $nr_nodes`; do
  cat ${CONFDIR}/cassandra.yaml.${i} |\
  awk -v addr=192.168.56.1${i} '{
     if($1=="listen_address:"){
        print $1, addr;
     }else if($1=="rpc_address:"){
        print $1, "0.0.0.0";
     }else{
        print $0
     }
  }' > ${CONFDIR}/.cassandra.yaml.${i}
  mv ${CONFDIR}/.cassandra.yaml.${i} ${CONFDIR}/cassandra.yaml.${i}
done

rm ${CONFDIR}/cassandra.yaml

# zoo.cfg
cp $CONF_TEMPLATE_DIR/zoo.cfg $CONFDIR/zoo.cfg
for i in `seq 1 $nr_nodes` ; do
  echo "server.${i}=onosdev${i}:2888:3888" >> $CONFDIR/zoo.cfg
done

# Set replication factor in cassandra.titan & titan-embedded.properties
if [ $nr_nodes -ge 3 ]; then
  rep_factor=3
else
  rep_factor=$nr_nodes
fi
for f in cassandra.titan titan-embedded.properties; do
  cat ${CONF_TEMPLATE_DIR}/$f | sed "s/storage\.replication-factor=[0-9]*/storage\.replication-factor=$rep_factor/g" > ${CONFDIR}/$f
done

## Set config.json
`dirname $0`/config_json.py $nr_nodes ${CONF_TEMPLATE_DIR}/config.json > ${CONFDIR}/config.json

function cp_print {
  echo "copying $1 $2"
  cp $1 $2
}

### Copy config files
echo "Copying confguration files"
CONF="zoo.cfg cassandra.titan titan-embedded.properties"
for i in ${CONF} ; do
  cp_print ${CONFDIR}/$i ${ONOS_HOME}/conf
done
for i in ${CONFDIR}/cassandra.yaml.* ; do
   cp_print $i ${ONOS_HOME}/conf
done

cp_print ${CONFDIR}/config.json        ${ONOS_HOME}/web
cp_print ${CONF_COMMON_DIR}/configuration.json ${ONOS_HOME}/web/ons-demo/data
cp_print ${CONF_COMMON_DIR}/controllers.json   ${ONOS_HOME}/web/ons-demo/data
cp_print ${CONF_COMMON_DIR}/dev_network.py     ${ONOS_HOME}/test-network/mininet
cp_print ${CONF_COMMON_DIR}/link.json.dev      ${ONOS_HOME}/web/link.json
cp_print ${CONF_COMMON_DIR}/cluster.hosts      ${ONOS_HOME}/.cluster.hosts

echo "Copying cassandra.yaml file to all VMs"
for i in `seq 1 $nr_nodes`; do
  scp ${CONFDIR}/cassandra.yaml.$i onosdev${i}:${ONOS_USER_HOME}/${CASSANDRA_DIR_NAME}/conf/cassandra.yaml
  scp ${CONFDIR}/zoo.cfg onosdev${i}:${ONOS_USER_HOME}/${ZOOKEEPER_DIR_NAME}/conf/zoo.cfg
  scp ${CONFDIR}/onosrc onosdev${i}:${ONOS_USER_HOME}/.onosrc
  ssh onosdev${i} "sudo sh -c \"echo $i > ${ZK_DATADIR}/myid\""
done

if [ x$CASSANDRA_DATADIR != "x" ]; then
  echo "removing all data in ${CASSANDRA_DATADIR}/ [Y/n]?"
  while [ 1 ]; do
   read key
   if [ x${key} == "xY" ]; then
     for i in `seq 1 $nr_nodes`; do
       ssh onosdev${i} "sudo rm -rf ${CASSANDRA_DATADIR}/*"
     done
     break
   elif [ x${key} == "xn" ]; then
     break
   else
     echo "[Y/n]?"
   fi
   sleep 1
  done
fi

for i in `seq 1 $nr_nodes`; do
  hosts=""
  for i in `seq 1 $nr_nodes`; do
    hosts="$hosts onosdev${i}"
  done
  ssh onosdev${i} "ssh-keyscan -t rsa $hosts >> \${HOME}/.ssh/known_hosts"
done
