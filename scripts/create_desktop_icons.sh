#!/bin/bash

. `dirname $0`/../config.sh

DESKTOP=${ONOS_USER_HOME}/Desktop

mkdir -p ${DESKTOP}

cat > ${DESKTOP}/Eclipse << EOF
[Desktop Entry]
Encoding=UTF-8
Type=Application
Name=Eclipse
Name[en_US]=Eclipse
Icon=/opt/eclipse/icon.xpm
Exec=/usr/bin/eclipse
Comment[en_US]=
EOF

cat > ${DESKTOP}/Terminal << EOF
[Desktop Entry]
Encoding=UTF-8
Type=Application
Name=Terminal
Name[en_US]=Terminal
Icon=konsole
Exec=/usr/bin/x-terminal-emulator
Comment[en_US]=
EOF

cat > ${DESKTOP}/Gerrit << EOF
[Desktop Entry]
Encoding=UTF-8
Type=Application
Name=ONOS Gerrit
Name[en_US]=ONOS Gerrit
Icon=internet-web-browser
Exec=/usr/bin/chromium-browser http://gerrit.onos.onlab.us
Comment[en_US]=
EOF

cat > ${DESKTOP}/Docs << EOF
[Desktop Entry]
Encoding=UTF-8
Type=Application
Name=ONOS Docs
Name[en_US]=ONOS Docs
Icon=internet-web-browser
Exec=/usr/bin/chromium-browser http://docs.onos.onlab.us
Comment[en_US]=
EOF

cat > ${DESKTOP}/Wireshark << EOF
[Desktop Entry]
Encoding=UTF-8
Type=Application
Name=Wireshark
Name[en_US]=Wireshark
Icon=wireshark
Exec=/usr/bin/wireshark
Comment[en_US]=
EOF

