#!/bin/bash

. `dirname $0`/../config.sh
. `dirname $0`/./util.sh

cd ${ONOS_USER_HOME}
tar xf ${ARCHIVE_DIR}/${MAVEN_ARCHIVE_NAME} --overwrite

#export M2_HOME=${ONOS_USER_HOME}/${MAVEN_DIR_NAME}
#export M2=${M2_HOME}/bin
#export PATH=${M2}:${PATH}

## Writing Maven config to ~/.onosrc (but this will be overwritten by conf_setup.sh later) ##
echo "export M2_HOME=\${HOME}/${MAVEN_DIR_NAME}" >> ${ONOS_USER_HOME}/.onosrc
echo "export M2=\${M2_HOME}/bin" >> ${ONOS_USER_HOME}/.onosrc
echo "export PATH=\${M2}:${PATH}" >> ${ONOS_USER_HOME}/.onosrc

cp ${SHARED_DIR}/conf_common/settings.xml ${ONOS_USER_HOME}/${MAVEN_DIR_NAME}/conf
