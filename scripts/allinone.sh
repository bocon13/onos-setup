#!/bin/bash

. `dirname $0`/../config.sh

FLAGFILE=${ONOS_USER_HOME}/.vagrant_onos.provisioned

if [ ! -f $FLAGFILE ]; then
  cd `dirname $0`
  sudo -u ${ONOS_USER} ./install_zookeeper.sh 
  sudo -u ${ONOS_USER} ./install_cassandra.sh 
  sudo -u ${ONOS_USER} ./install_onos.sh 
  sudo -u ${ONOS_USER} ./install_maven.sh 
  sudo -u ${ONOS_USER} ./post_install.sh  
  touch $FLAGFILE 
fi
