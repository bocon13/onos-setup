#!/bin/bash

. `dirname $0`/../config.sh

# Download cassandra
wget -c -N ${CASSANDRA_MIRROR_URL} -P ${ARCHIVE_DIR}

# Download Zookeeper
wget -c -N ${ZOOKEEPER_MIRROR_URL} -P ${ARCHIVE_DIR}

# Download Maven
wget -c -N ${MAVEN_MIRROR_URL} -P ${ARCHIVE_DIR}

