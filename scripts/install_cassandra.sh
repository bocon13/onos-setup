#!/bin/bash

. `dirname $0`/../config.sh

sudo apt-get -qq update
sudo apt-get -qqy install ${JDK_PKG}

if [ ! -f ${ARCHIVE_DIR}/${CASSANDRA_ARCHIVE_NAME} ]; then
  . ${SHARED_DIR}/scripts/fetch_preqs.sh
fi

cd ${ONOS_USER_HOME}  # cd to ONOS_USER's home directory 
tar xf ${ARCHIVE_DIR}/${CASSANDRA_ARCHIVE_NAME} --no-same-owner

if [ ! -d ${CASSANDRA_DATADIR} ]; then
  sudo mkdir -p ${CASSANDRA_DATADIR}
  sudo chown -R `whoami` ${CASSANDRA_DATADIR}
fi
if [ ! -d ${CASSANDRA_LOGDIR} ]; then
  sudo mkdir -p ${CASSANDRA_LOGDIR}
  sudo chown -R `whoami` ${CASSANDRA_LOGDIR}
fi
