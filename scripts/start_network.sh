#! /bin/sh
if [ ! "$#" -eq 2 ]; then
  echo "Usage: $0 nr_nodes {start|stop}"
  exit
fi
nr_nodes=$1

dir=`dirname $0`
. ${dir}/../config.sh

start (){
  for i in `seq 1 $nr_nodes`; do
    ssh onosdev${i} "./ONOS/test-network/mininet/dev_network_tunnel_${i}.sh stop; ./ONOS/test-network/mininet/dev_network_tunnel_${i}.sh start"
  done
  if [ $nr_nodes -eq 1 ]; then
    ssh onosdev1 "sudo mn -c ; sudo ./ONOS/test-network/mininet/dev_network_single.py -n" &
    cp ${dir}/../conf_common/link.json.single ${dir}/../ONOS/web/link.json
  else
    cp ${dir}/../conf_common/link.json.cluster ${dir}/../ONOS/web/link.json
    ssh onosdev1 "sudo mn -c ; sudo ./ONOS/test-network/mininet/dev_network_core.py -n" &
    for i in `seq 2 $nr_nodes`; do
      ssh onosdev${i} "sudo mn -c ; sudo ./ONOS/test-network/mininet/dev_network_edge.py ${i} 25" &
    done
  fi
}
stop (){
  for i in `seq 1 $nr_nodes`; do
    ssh onosdev${i} "./ONOS/test-network/mininet/dev_network_tunnel_${i}.sh stop" &
  done
  for i in `seq 1 $nr_nodes`; do
      ssh onosdev${i} "sudo mn -c" &
  done
}

case "$2" in
  start)
    stop
    start 
    ;;
  stop)
    stop
    ;;
  *)
    echo "Usage: $0 {start|stop}"
    exit 1
esac
