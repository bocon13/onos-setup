#! /usr/bin/env python
import sys
import re
import os

if len(sys.argv) != 4:
  print "Usage: %s <nr_node> <cassandra.yaml> <output_dir>" % sys.argv[0]
  sys.exit(1)

nr_node=int(sys.argv[1])
tempyaml=str(sys.argv[2])
outdir=str(sys.argv[3])

tokens=[]
if nr_node == 1:
 tokens.append(0)
else:
  for i in range(nr_node):
    tokens.append(((2**64 / nr_node) * i) - 2**63)

for i in range(len(tokens)):
  infile  = open(tempyaml, 'r')
  outfile = open("%s/cassandra.yaml." % (outdir) + str(i+1), 'w')
  for line in infile:
    if re.match("initial_token:",line):
      line=("initial_token: " + str(tokens[i]) + "\n")
    outfile.write(line)  

  infile.close()
  outfile.close()
